package ma.octo.assignement.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;

@Entity
@Table(name = "VERSEMENTS")
public class Versement {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(precision = 16, scale = 2, nullable = false)
  private BigDecimal montantVersement;

  @Column
  private Instant dateExecution;

  @Column
  private String nomPrenomEmetteur;

  public BigDecimal getMontantVersement() {
    return montantVersement;
  }

  public void setMontantVersement(BigDecimal montantVersement) {
    this.montantVersement = montantVersement;
  }

  public String getNomPrenomEmetteur() {
    return nomPrenomEmetteur;
  }

  public void setNomPrenomEmetteur(String nomPrenomEmetteur) {
    this.nomPrenomEmetteur = nomPrenomEmetteur;
  }

  @OneToOne
  private Compte compteBeneficiaire;

  @Column(length = 200)
  private String motifVersement;

  public BigDecimal getMontantVirement() {
    return montantVersement;
  }

  public void setMontantVirement(BigDecimal montantVirement) {
    this.montantVersement = montantVirement;
  }

  public Instant getDateExecution() {
    return dateExecution;
  }

  public void setDateExecution(Instant dateExecution) {
    this.dateExecution = dateExecution;
  }

  public Compte getCompteBeneficiaire() {
    return compteBeneficiaire;
  }

  public void setCompteBeneficiaire(Compte compteBeneficiaire) {
    this.compteBeneficiaire = compteBeneficiaire;
  }

  public String getMotifVersement() {
    return motifVersement;
  }

  public void setMotifVersement(String motifVirement) {
    this.motifVersement = motifVirement;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNom_prenom_emetteur() {
    return nomPrenomEmetteur;
  }

  public void setNom_prenom_emetteur(String nom_prenom_emetteur) {
    this.nomPrenomEmetteur = nom_prenom_emetteur;
  }
}
