package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;

public class VersementMapper {

    public static VersementDto map(Versement versement) {
        return new VersementDto(
                versement.getMontantVersement(),
                versement.getDateExecution(),
                versement.getNomPrenomEmetteur(),
                versement.getCompteBeneficiaire().getRib(),
                versement.getMotifVersement()
        );
    }
}
