package ma.octo.assignement.controller;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UtilisateurController {

    @Autowired
    UtilisateurService utilisateurService;

    @GetMapping("/utilisateurs")
    List<Utilisateur> loadAllUtilisateur() {
        return utilisateurService.getAllUtilisateurs();
    }

}
