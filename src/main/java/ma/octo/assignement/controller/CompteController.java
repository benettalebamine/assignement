package ma.octo.assignement.controller;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.service.CompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CompteController {

    @Autowired
    CompteService compteService;

    @GetMapping("/comptes")
    List<Compte> loadAllCompte() {
        return compteService.getAllComptes();
    }

}
