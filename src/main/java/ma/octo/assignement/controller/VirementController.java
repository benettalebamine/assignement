package ma.octo.assignement.controller;

import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.service.VirementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@RestController
public class VirementController {

    @Autowired
    private VirementService virementService;

    @GetMapping("/virements")
    List<VirementDto> loadAllVirements() {
        return virementService.getAllVirements();
    }

    @PostMapping("/virement")
    @ResponseStatus(HttpStatus.CREATED)
    public void createVirement(
            @Valid @RequestParam @NotEmpty(message = "Pas de compte emetteur") String numeroCompteEmetteur,
            @Valid @RequestParam @NotEmpty(message = "Pas de compte beneficiaire") String numeroCompteBeneficiaire,
            @Valid @RequestParam @NotEmpty(message = "Pas de motif") String motif,
            @Valid @RequestParam @Min(value = 10, message = "Montant minimale non atteint (10)") @Max(value = 10000, message = "Montant maximale atteint (10000)") BigDecimal montantVirement,
            @RequestParam Instant date
    ) throws SoldeDisponibleInsuffisantException, CompteNonExistantException {
        virementService.createVirement(new VirementDto(numeroCompteEmetteur, numeroCompteBeneficiaire, motif, montantVirement, date));
    }
}
