package ma.octo.assignement.controller;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.VersementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@RestController(value = "/versements")
public class VersementController {

    @Autowired
    VersementService versementService;

    @GetMapping("listeversements")
    List<VersementDto> loadAllVersements() {
        return versementService.getAllVersements();
    }

    @PostMapping("/versement")
    @ResponseStatus(HttpStatus.CREATED)
    public void createVersement(
            @Valid @RequestParam @NotEmpty(message = "Pas du nom emetteur") String nomPrenomEmetteur,
            @Valid @RequestParam @NotEmpty(message = "Pas de compte beneficiaire") String rib,
            @Valid @RequestParam @NotEmpty(message = "Pas de motif") String motifVersement,
            @Valid @RequestParam @Min(value = 10, message = "Montant minimale non atteint (10)") @Max(value = 10000, message = "Montant maximale atteint (10000)") BigDecimal montantVersement,
            @RequestParam Instant dateExecution
    ) throws CompteNonExistantException {
        versementService.createVersement(new VersementDto(montantVersement, dateExecution, nomPrenomEmetteur, rib, motifVersement));
    }
}
