package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.VersementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class VersementService {

    Logger LOGGER = LoggerFactory.getLogger(VirementService.class);

    @Autowired
    private VersementRepository versementRepository;
    @Autowired
    private CompteService compteService;
    @Autowired
    private AuditService auditService;


    public List<VersementDto> getAllVersements() {
        return versementRepository
                .findAll()
                .stream()
                .map(versement -> VersementMapper.map(versement))
                .collect(Collectors.toList());
    }

    public void createVersement(VersementDto versementDto) throws CompteNonExistantException {
        Optional<Compte> optBeneficiaire = compteService.findByRib(versementDto.getRib());

        if (optBeneficiaire.isEmpty()) {
            LOGGER.error("beneficiaire Non existant");
            throw new CompteNonExistantException("beneficiaire Non existant");
        }
        Compte beneficiaire = optBeneficiaire.get();

        beneficiaire.setSolde(beneficiaire.getSolde().add(versementDto.getMontantVersement()));
        compteService.save(beneficiaire);

        Versement versement = new Versement();
        versement.setDateExecution(versementDto.getDateExecution());
        versement.setCompteBeneficiaire(beneficiaire);
        versement.setMontantVersement(versementDto.getMontantVersement());
        versement.setMotifVersement(versementDto.getMotifVersement());

        versementRepository.save(versement);

        auditService.audit("Versement par " + versementDto.getNomPrenomEmetteur() + " vers " + versementDto.getRib()
                + " d'un montant de " + versementDto.getMontantVersement()
                .toString(), EventType.VERSEMENT);
    }
}
