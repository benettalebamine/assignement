package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.VirementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class VirementService {





    Logger LOGGER = LoggerFactory.getLogger(VirementService.class);

    @Autowired
    private VirementRepository virementRepository;
    @Autowired
    private CompteService compteService;
    @Autowired
    private AuditService auditService;


    public List<VirementDto> getAllVirements() {
        return virementRepository
                .findAll()
                .stream()
                .map(virement -> VirementMapper.map(virement))
                .collect(Collectors.toList());
    }

    public void createVirement(VirementDto virementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException {
        Optional<Compte> optEmetteur = compteService.findByNumeroCompte(virementDto.getNumeroCompteEmetteur());
        Optional<Compte> optBeneficiaire = compteService.findByNumeroCompte(virementDto.getNumeroCompteBeneficiaire());

        if (optEmetteur.isEmpty()) {
            LOGGER.error("emetteur Non existant");
            throw new CompteNonExistantException("emetteur Non existant");
        }

        if (optBeneficiaire.isEmpty()) {
            LOGGER.error("beneficiaire Non existant");
            throw new CompteNonExistantException("beneficiaire Non existant");
        }
        Compte emetteur = optEmetteur.get();
        Compte beneficiaire = optBeneficiaire.get();
        int montantIsEnough = emetteur.getSolde().compareTo(virementDto.getMontantVirement());
        if (montantIsEnough == -1) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }

        emetteur.setSolde(emetteur.getSolde().subtract(virementDto.getMontantVirement()));
        compteService.save(emetteur);

        beneficiaire.setSolde(beneficiaire.getSolde().add(virementDto.getMontantVirement()));
        compteService.save(beneficiaire);

        Virement virement = new Virement();
        virement.setDateExecution(virementDto.getDate());
        virement.setCompteBeneficiaire(beneficiaire);
        virement.setCompteEmetteur(emetteur);
        virement.setMontantVirement(virementDto.getMontantVirement());
        virement.setMotifVirement(virementDto.getMotif());

        virementRepository.save(virement);

        auditService.audit("Virement depuis " + virementDto.getNumeroCompteEmetteur() + " vers " + virementDto
                .getNumeroCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                .toString(), EventType.VIREMENT);
    }
}
