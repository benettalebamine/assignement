package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;

public class VirementDto {
  private String NumeroCompteEmetteur;
  private String NumeroCompteBeneficiaire;
  private String motif;
  private BigDecimal montantVirement;
  private Instant date;

  public VirementDto(String numeroCompteEmetteur, String numeroCompteBeneficiaire, String motif, BigDecimal montantVirement, Instant date) {
    NumeroCompteEmetteur = numeroCompteEmetteur;
    NumeroCompteBeneficiaire = numeroCompteBeneficiaire;
    this.motif = motif;
    this.montantVirement = montantVirement;
    this.date = date;
  }

  public VirementDto() {
  }

  public String getNumeroCompteEmetteur() {
    return NumeroCompteEmetteur;
  }

  public void setNumeroCompteEmetteur(String numeroCompteEmetteur) {
    NumeroCompteEmetteur = numeroCompteEmetteur;
  }

  public String getNumeroCompteBeneficiaire() {
    return NumeroCompteBeneficiaire;
  }

  public void setNumeroCompteBeneficiaire(String numeroCompteBeneficiaire) {
    NumeroCompteBeneficiaire = numeroCompteBeneficiaire;
  }

  public String getMotif() {
    return motif;
  }

  public void setMotif(String motif) {
    this.motif = motif;
  }

  public BigDecimal getMontantVirement() {
    return montantVirement;
  }

  public void setMontantVirement(BigDecimal montantVirement) {
    this.montantVirement = montantVirement;
  }

  public Instant getDate() {
    return date;
  }

  public void setDate(Instant date) {
    this.date = date;
  }
}
