package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.time.Instant;

public class VersementDto {

    private BigDecimal montantVersement;
    private Instant dateExecution;
    private String nomPrenomEmetteur;
    private String rib;
    private String motifVersement;

    public VersementDto(BigDecimal montantVersement, Instant dateExecution, String nomPrenomEmetteur, String rib, String motifVersement) {
        this.rib = rib;
        this.dateExecution = dateExecution;
        this.motifVersement = motifVersement;
        this.montantVersement = montantVersement;
        this.nomPrenomEmetteur = nomPrenomEmetteur;
    }

    public BigDecimal getMontantVersement() {
        return montantVersement;
    }

    public Instant getDateExecution() {
        return dateExecution;
    }

    public String getNomPrenomEmetteur() {
        return nomPrenomEmetteur;
    }

    public String getRib() {
        return rib;
    }

    public String getMotifVersement() {
        return motifVersement;
    }
}
