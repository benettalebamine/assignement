package ma.octo.assignement.serviceTest;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.VersementService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class VersementServiceTest {

    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private VersementService versementService;
    @Autowired
    private AuditService auditService;

    Compte compte1 = new Compte();


    @BeforeEach
    void setUp() {
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUserName("amine");
        utilisateur1.setLastName("last1");
        utilisateur1.setFirstName("first1");
        utilisateur1.setGender("Male");
        utilisateur1.setBirthDate(new Date());


        utilisateurRepository.save(utilisateur1);


        compte1.setNumeroCompte("010000A0000010");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(2000L));
        compte1.setUtilisateur(utilisateur1);

        compteRepository.save(compte1);
    }

    @Test
    void createVersementTest() throws CompteNonExistantException {
        BigDecimal solde = compte1.getSolde();
        versementService.createVersement(new VersementDto(BigDecimal.valueOf(100), Instant.now(), "amine benettaleb", "RIB1", "PERSO"));
        BigDecimal soldeAfter = new BigDecimal(100);
        soldeAfter = soldeAfter.setScale(2);
        Assertions.assertEquals(solde.add(soldeAfter), compteRepository.findByNumeroCompte("010000A0000010").get().getSolde());
        // Check AUdit
        List<Audit> audits = auditService.getAllAudits();
        Assertions.assertEquals("Versement par amine benettaleb vers RIB1 d'un montant de 100", audits.get(0).getMessage());
    }
}